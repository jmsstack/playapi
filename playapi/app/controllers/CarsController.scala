package controllers

//import io.circe._, io.circe.parser_
import javax.inject._
import play.api._
import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms._
import java.{util => ju}
import models.{Car, CarDto}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import play.api.libs.circe.Circe
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe._
import io.circe.parser._
import services.CarsService
import com.mongodb.MongoException

@Singleton
class CarsController @Inject()(val controllerComponents: ControllerComponents, val carsService: CarsService) extends BaseController with Circe {

  def getById(id: Int) = Action { implicit request: Request[AnyContent] =>
    try {
      val car = carsService.getItemById(id)
      Ok(car.asJson)
    }
    catch {
      case e:MongoException => NotFound(e.getMessage())
      case _: Throwable => BadRequest("Unknown error")
    }
  }

  def get(sortBy: Option[String] = null) = Action { implicit request: Request[AnyContent] =>
    try{
      val res = carsService.getAll(sortBy).asJson        
      Ok(res)    
    }
    catch{
      case _: Throwable => BadRequest("Unknown error")
    }

  }

  def add() = Action(circe.json[CarDto]) { implicit request =>
    try {      
      val car = request.body.toModel()
      carsService.add(car)
      Ok(car.toDto().asJson)
    }
    catch{
      case e: IllegalArgumentException => UnprocessableEntity(e.getMessage())
      case e: Exception => BadRequest(e.getMessage())
      case _: Throwable => BadRequest("Unknown error")
    }
  }

  def update(id: Int) = Action(circe.json[CarDto]) { implicit request =>
    try{
      val car = request.body.toModel()  
      carsService.update(id, car)
      Ok(car.toDto().asJson)
    }
    catch{
      case e: IllegalArgumentException => UnprocessableEntity(e.getMessage())
      case e: MongoException => NotFound(e.getMessage())
      case _: Throwable => BadRequest("Unknown error")
    }
  }

  def delete(id: Int) = Action { implicit request =>
    try{
      carsService.delete(id)
      Ok("")
    }
    catch{      
      case e: MongoException => NotFound(e.getMessage())
      case _: Throwable => BadRequest("Unknown error")
    }
  }
    
}
