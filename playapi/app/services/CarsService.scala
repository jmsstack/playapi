package services

import models.{Car}

import javax.inject.Inject
import play.api.Configuration

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Aggregates._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.model._
import scala.collection.JavaConverters._
import scala.Exception
import scala.collection.immutable.IndexedSeq


import java.util.concurrent.TimeUnit

import scala.concurrent.Await
import scala.concurrent.duration.Duration

import org.bson.UuidRepresentation
import org.bson.codecs.UuidCodec
import org.bson.codecs.configuration.CodecRegistries

import org.mongodb.scala.bson.codecs._
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistry
import org.bson.codecs.configuration.CodecRegistries.{fromRegistries, fromProviders, fromCodecs}
import models.Car

import org.bson.BsonReader
import org.bson.BsonWriter
import org.joda.time.DateTime
import org.bson.codecs.EncoderContext
import org.bson.codecs.DecoderContext
import org.bson.codecs.Codec
import models.JodaCodec
import models.CarDto
import com.mongodb.client.result.DeleteResult




/**
 * Storage exception.
 *
 * @param message exception message
 * @param cause   exception cause
 */
class StorageException(message: String, cause: Throwable) extends Exception(message, cause)

/**
 * Exception thrown on bad id inputs.
 *
 * @param id      the id responsible for the exception
 * @param cause   exception cause
 */
class BadIdException(id: String, cause: Throwable) extends StorageException("bad id: " + id, cause)


class CarsService @Inject()(configuration: Configuration) {

  implicit class DocumentObservable[C](val observable: Observable[Document]) extends ImplicitObservable[Document] {
    override val converter: (Document) => String = (doc) => doc.toJson
  }

  implicit class GenericObservable[C](val observable: Observable[C]) extends ImplicitObservable[C] {
    override val converter: (C) => String = (doc) => doc.toString
  }

  trait ImplicitObservable[C] {
    val observable: Observable[C]
    val converter: (C) => String

    def results(): Seq[C] = Await.result(observable.toFuture(), Duration(10, TimeUnit.SECONDS))
    def headResult() = Await.result(observable.head(), Duration(10, TimeUnit.SECONDS))
    def printResults(initial: String = ""): Unit = {
      if (initial.length > 0) print(initial)
      results().foreach(res => println(converter(res)))
    }
    def printHeadResult(initial: String = ""): Unit = println(s"${initial}${converter(headResult())}")
  }


    val carCodecProcvider: CodecProvider = Macros.createCodecProviderIgnoreNone[Car]()
    val codecRegistry: CodecRegistry = CodecRegistries.fromRegistries(fromProviders(carCodecProcvider), CodecRegistries.fromCodecs(new JodaCodec), MongoClient.DEFAULT_CODEC_REGISTRY)

    val settings = MongoClientSettings.builder()
                  .codecRegistry(codecRegistry).build()
                  

  val mongoClient = MongoClient(configuration.get[String]("mongodb.uri"))
  val database: MongoDatabase = mongoClient.getDatabase("cars_DB")
  val collection: MongoCollection[Car] = database.getCollection[Car]("cars").withCodecRegistry(codecRegistry)

  def getAll(sortBy: Option[String] = null): Seq[CarDto] = {
    var sort: String = "id"
    if(sortBy.exists(_.trim.nonEmpty)){
      sort = sortBy.get
    }
    val cars = collection.find[Car]().sort(ascending(sort)).results()
    val carDtos = cars.map(c => CarDto(c.id, c.title, c.fuelType, c.price, c.isNew, c.mileage, c.firstRegistration.toString()))
    return carDtos
  }

  def getItemById(id: Int): CarDto = {
    val cars = collection.find[Car](equal("id", id)).results()
    if(cars.isEmpty)
    {
      throw new MongoException("No car advert with that Id has been found!")
    }
    return cars.head.toDto()
  }

  def add(car: Car) = {  
    val error = car.getValidationError()  
    if(error != ""){
      throw new IllegalArgumentException(error)
    }
    val result = collection.insertOne(car)
  }

  def update(id: Int, car: Car) = {
    getItemById(id)
    val error = car.getValidationError()  
    if(error != ""){
      throw new IllegalArgumentException(error)
    }
    collection.updateOne(
                  equal("id", id),                
                  combine(set("title", car.title),set("fuelType", car.fuelType),set("price", car.price),set("isNew", car.isNew),set("mileage", car.mileage),set("firstRegistration", car.firstRegistration)))
    
  }

  def delete(id: Int) = {
    getItemById(id)
    collection.deleteOne(equal("id", id))
  }

}
