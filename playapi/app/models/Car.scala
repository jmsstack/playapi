package models

import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms._
import org.joda.time.DateTime
import org.bson.codecs.DecoderContext
import org.bson.codecs.Codec
import org.bson.BsonReader
import org.bson.BsonWriter
import org.bson.codecs.EncoderContext

case class Car (id: Int, title: String, fuelType: String, price: Double, isNew: Boolean, mileage: Int, firstRegistration: DateTime){
  def toDto() : CarDto = {
    val car = new CarDto(this.id, this.title, this.fuelType, this.price, this.isNew, this.mileage, this.firstRegistration.toString())
    return car
  }

  def getValidationError(): String ={
    if(this.id <= 0)
    {
      return "Id must be a positive number";
    }
    if(this.price < 0)
    {
      return "Price cannot be negative!"
    }
    return ""
  }
}


case class CarDto(id: Int, title: String, fuelType: String, price: Double, isNew: Boolean, mileage: Int, firstRegistration: String){
  def toModel() : Car = {
    val car = new Car(this.id, this.title, this.fuelType, this.price, this.isNew, this.mileage, DateTime.parse(this.firstRegistration))
    return car
  }

  def fromModel(car: Car): CarDto = {
    var dto = new CarDto(car.id, car.title, car.fuelType, car.price, car.isNew, car.mileage, car.firstRegistration.toString())
    return dto
  }    
}
case class JodaCodec() extends Codec[DateTime] {
  override def decode(bsonReader: BsonReader, decoderContext: DecoderContext): DateTime = new DateTime(bsonReader.readDateTime())
  override def encode(bsonWriter: BsonWriter, t: DateTime, encoderContext: EncoderContext): Unit = bsonWriter.writeDateTime(t.getMillis)
  override def getEncoderClass: Class[DateTime] = classOf[DateTime]
}
